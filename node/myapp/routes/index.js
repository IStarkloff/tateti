var express = require('express');
var router = express.Router();
var redis = require("redis"),
  client = redis.createClient();


client.on('connect', function () {
  console.log('Conectado a Redis Server');
});
client.on("error", function (error) {
  console.error("error al conectar con la base de datos: " + error);
});
router.get('/', function (req, res, next) {
  try {
    res.json({ status: 200, message: 'juego levantado' });
  } catch (err) {
    res.json({ status: 400, message: 'error en levantar juego: ' + err });
  }
});
router.post('/players', (req, res, next) => {
  try {
    client.hgetall(`playerString#${req.body.idPlayer}`, (err, checkPlayer) => {
      if (checkPlayer){
        res.json({ status: 200, message: 'jugador ' + req.body.idPlayer + " ya existe" });
      } else if (err){
        res.json({ status: 500, message: 'jugador ' + req.body.idPlayer + " no se pudo verificar " + err });
      } else{
        var player = {
          idPlayer: req.body.idPlayer,
          namePlayer: req.body.namePlayer
        }
        let id = req.body.idPlayer;
        let playerString = JSON.stringify(player);
        client.hmset(`playerString#${id}`, "player", playerString, (err, result) => {
          if (result) {
            res.json({ status: 200, message: 'jugador ' + req.body.idPlayer + " creado" });
          } else {
            res.json({ status: 500, message: 'error al crear el jugador en base de datos: ' + err });
          }
        });
      }
    })
    
  } catch (err) {
    res.json({ status: 400, message: 'error al crear al jugador, no se puedo conectar al servidor: ' + err });
  }
});

// router.get("/players/:idPlayer", (req, res, next) => {
//   try {

//   } catch (err) {

//   }

// });

router.post("/boards", (req, res, next) => {
  try {
    client.hgetall(`playerString#${req.body.idPlayer}`, (err, checkPlayer) => {
      if (checkPlayer) {
        var board = {
          idBoard: req.body.idBoard,
          idPlayer1: req.body.idPlayer,
          idPlayer2: "",
          position0: "",
          position1: "",
          position2: "",
          position3: "",
          position4: "",
          position5: "",
          position6: "",
          position7: "",
          position8: "",
          boardState: "Iniciado",
          playerTurn: req.body.idPlayer
        }
        let id = req.body.idBoard;
        let boardString = JSON.stringify(board);
        client.hmset(`boardString#${id}`, "board", boardString, (err, result) => {
          if (result) {
            res.json({ status: 200, message: 'tablero ' + boardString + " creado" });
          } else if (err) {
            res.json({ status: 500, message: 'error al crear el tablero en base de datos: ' + err });
          }
        });
      } else if (err) {
        res.json({ status: 500, message: 'no se encontro un jugador con el id especificado en la base de datos: ' + err });
      } else {
        res.json({ status: 500, message: 'no se encontro un jugador con el id especificado en la base de datos' });
      }
    });
  } catch (err) {
    res.json({ status: 400, message: 'error al crear al tablero, no se puedo conectar al servidor: ' + err });
  }

});

router.put("/boards/:idBoard/join", (req, res, next) => {
  try {
    client.hgetall(`playerString#${req.body.idPlayer2}`, (err, checkPlayer) => {
      if (checkPlayer) {
        client.hget(`boardString#${req.params.idBoard}`, 'board', (err, result) => {
          if (result) {
            let board = JSON.parse(result);
            console.log(board);
            if (board["boardState"] == "Iniciado" || board["idPlayer2"] != "") {
              if (board["idPlayer1"] == req.body.idPlayer2) {
                res.json({ state: 200, message: "los id de los jugadores no pueden ser iguales" });
              } else {
                board["idPlayer2"] = req.body.idPlayer2;
                board["boardState"] = "completo"
                boardString = JSON.stringify(board)
                client.hmset(`boardString#${req.params.idBoard}`, "board", boardString, (err, resultSet) => {
                  if (resultSet) {
                    res.json({ state: 200, message: "se ha unido a la partida correctamente " + board["idPlayer2"] });
                  } else if (err) {
                    res.json({ state: 500, message: "no se pudo unir a la partida, error en base de datos: " + err });
                  }
                });
              }
            } else {
              res.json({ state: 200, message: "la partida ya esta completa, no se puede unir" });
            }
          } else if (err) {
            res.json({ status: 500, message: "Error al obtener el board de la base de datos: " + err });
          } else {
            res.json({ status: 200, message: "No se encontro la partida buscada" });
          }
        });
      } else if (err) {
        res.json({ status: 500, message: 'no se encontro un jugador con el id especificado en la base de datos: ' + err });
      } else {
        res.json({ status: 500, message: 'no se encontro un jugador con el id especificado en la base de datos' });
      }
    });
  } catch (err) {
    res.json({ status: 400, message: "Error al unirse a la partida, no se pudo conectar al servidor: " + err });
  }
});


router.put("/boards/:idBoard/movement", async (req, res, next) => {
  try {
    client.hget(`boardString#${req.params.idBoard}`, 'board', (err, result) => {
      if (result) {
        console.log(result);
        let board = JSON.parse(result);
        // un poco harcodeado pero lo que hace es ver en que posicion colocar el id del jugador que hace el movimiento,
        if (board["boardState"] == "completo") {
          if (board["playerTurn"] == board["idPlayer1"]) {
            if (req.body.position >= 0 && req.body.position <= 8) {
              for (var i = 0; i < 9; i++) {
                if (req.body.position == i) {
                  var contadorString = i.toString();
                  if (board["position" + contadorString] == "") {
                    board["position" + contadorString] = board["idPlayer1"];
                    board["playerTurn"] = board["idPlayer2"];
                    if (board["position0"] == board["idPlayer1"] && board["position1"] == board["idPlayer1"] && board["position2"] == board["idPlayer1"] ||
                      board["position3"] == board["idPlayer1"] && board["position4"] == board["idPlayer1"] && board["position5"] == board["idPlayer1"] ||
                      board["position6"] == board["idPlayer1"] && board["position7"] == board["idPlayer1"] && board["position8"] == board["idPlayer1"] ||
                      board["position0"] == board["idPlayer1"] && board["position3"] == board["idPlayer1"] && board["position6"] == board["idPlayer1"] ||
                      board["position1"] == board["idPlayer1"] && board["position4"] == board["idPlayer1"] && board["position7"] == board["idPlayer1"] ||
                      board["position2"] == board["idPlayer1"] && board["position5"] == board["idPlayer1"] && board["position8"] == board["idPlayer1"] ||
                      board["position0"] == board["idPlayer1"] && board["position4"] == board["idPlayer1"] && board["position8"] == board["idPlayer1"] ||
                      board["position2"] == board["idPlayer1"] && board["position4"] == board["idPlayer1"] && board["position6"] == board["idPlayer1"]) {
                      board["boardState"] = "Finalizado";
                      boardString = JSON.stringify(board);
                      client.hmset(`boardString#${req.params.idBoard}`, "board", boardString, (err, result) => {
                        if (result) {
                          res.json({ status: 200, message: "el jugador con id: " + board["idPlayer1"] + " gano, Un crack" });
                        }
                        else if (err) {
                          res.json({ status: 500, message: "error al realizar el movimiento en la base de datos: " + err });
                        }
                        else {
                          res.json({ status: 500, message: "error al realizar el movimiento en la base de datos a la hora de verificar ganador" });
                        }
                      });
                    }
                    else {
                      boardString = JSON.stringify(board);
                      client.hmset(`boardString#${req.params.idBoard}`, "board", boardString, (err, resultSet) => {
                        if (resultSet) {
                          res.json({ status: 200, message: "se ha realizado el movimiento " });
                        }
                        else if (err) {
                          res.json({ status: 500, message: "error al realizar el movimiento en la base de datos: " + err });
                        }
                        else {
                          res.json({ status: 500, message: "error al realizar el movimiento en la base de datos" });
                        }
                      });
                    }

                  }
                  else {
                    res.json({ status: 200, message: "la posicion " + contadorString + " ya esta ocupada" + board["idPlayer1"] });
                  }
                }
              }
            } else {
              res.json({ status: 200, message: "la posicion " + req.body.position + " no existe, tiene que ser en 0 y 9" });
            }
          }
          else if (board["playerTurn"] == board["idPlayer2"]) {
            if (req.body.position >= 0 && req.body.position <= 9) {
            for (var i = 0; i < 9; i++) {
              if (req.body.position == i) {
                var contadorString = i.toString();
                if (board["position" + contadorString] == "") {
                  board["position" + contadorString] = board["idPlayer2"];
                  board["playerTurn"] = board["idPlayer1"];
                  if (board["position0"] == board["idPlayer2"] && board["position1"] == board["idPlayer2"] && board["position2"] == board["idPlayer2"] ||
                    board["position3"] == board["idPlayer2"] && board["position4"] == board["idPlayer2"] && board["position5"] == board["idPlayer2"] ||
                    board["position6"] == board["idPlayer2"] && board["position7"] == board["idPlayer2"] && board["position8"] == board["idPlayer2"] ||
                    board["position0"] == board["idPlayer2"] && board["position3"] == board["idPlayer2"] && board["position6"] == board["idPlayer2"] ||
                    board["position1"] == board["idPlayer2"] && board["position4"] == board["idPlayer2"] && board["position7"] == board["idPlayer2"] ||
                    board["position2"] == board["idPlayer2"] && board["position5"] == board["idPlayer2"] && board["position8"] == board["idPlayer2"] ||
                    board["position0"] == board["idPlayer2"] && board["position4"] == board["idPlayer2"] && board["position8"] == board["idPlayer2"] ||
                    board["position2"] == board["idPlayer2"] && board["position4"] == board["idPlayer2"] && board["position6"] == board["idPlayer2"]) {
                    board["boardState"] = "Finalizado";
                    boardString = JSON.stringify(board);
                    client.hmset(`boardString#${req.params.idBoard}`, "board", boardString, (err, result) => {
                      if (result) {
                        return res.json({ status: 200, message: "el jugador con id: " + board["idPlayer2"] + " gano, Un crack" });
                      }
                      else if (err) {
                        res.json({ status: 500, message: "error al realizar el movimiento en la base de datos: " + err });
                      }
                      else {
                        res.json({ status: 500, message: "error al realizar el movimiento en la base de datos a la hora de verificar ganador" });
                      }
                    });
                  }
                  else {
                    boardString = JSON.stringify(board);
                    client.hmset(`boardString#${req.params.idBoard}`, "board", boardString, (err, result) => {
                      if (result) {
                        res.json({ status: 200, message: "se ha realizado el movimiento" });
                      }
                      else if (err) {
                        res.json({ status: 500, message: "error al realizar el movimiento en la base de datos: " + err });
                      }
                      else {
                        res.json({ status: 500, message: "error al realizar el movimiento en la base de datos" });
                      }
                    });
                  }
                }
                else {
                  res.json({ status: 200, message: "la posicion " + contadorString + " ya esta ocupada " + board["idPlayer2"] });
                }
              }
            }
          } else {
            res.json({ status: 200, message: "la posicion " + req.body.position + " no existe, tiene que ser en 0 y 9" });
          }
          }
          
          else {
            res.json({ status: 200, message: "la variable playerTurn debe contener el id de uno de los 2 jugadores que estan en la partida" });
          }
        }
        
        else {
          res.json({ status: 200, message: "la partida no esta en estado completo" });
        }
      }
      else if (err) {
        res.json({ status: 500, message: "error al buscar la partida en la base de datos: " + err });
      }
      else {
        res.json({ status: 200, message: "no se encuentra la partida especificada en la base de datos" });
      }
    });
  } catch (err) {
    res.json({ status: 400, message: "error al realizar el movimiento, no se pudo conectar con el servidor: " + err });
  }
});

router.get("/boards/:idBoard", (req, res, next) => {
  try {
    client.hget(`boardString#${req.params.idBoard}`, 'board', (err, result) => {
      if (result) {
        res.json({ status: 200, message: result })
      } else if (err) {
        res.json({ status: 500, message: "error al realizar el movimiento en la partida en la base de datos: " + err });
      } else {
        res.json({ status: 200, message: "no se encuentra la partida en la cual se quiere hacer un movimiento en la base de datos" });
      }
    })
  } catch (err) {
    res.json({ status: 400, message: "error al obtener el movimiento, no se pudo conectar con el servidor: " + err });
  }

});



module.exports = router;