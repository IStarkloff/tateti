# tateti
**Para el backEnd:**    
En un principio tenemos que levantar el backEnd que esta en la carpeta node 
Primero vamos a ejecutar el comando:    
`npm i`
vamos a la carpeta redis-4.0.1 y en la terminal ejecutamos el comando:  
`src/redis-server`  
despues salimos de la carpeta  redis-4.0.1 y vamos a la carpeta myapp y en la ejecutamos el comando:  
`npm i` 
y despues para levantarlo:
`DEBUG=myapp:* npm start`
Una vez realizado esos pasos el backEnd esta levantado  
**Para el frontEnd**    
despues de levantar el backEnd procedemos a ir a la carpeta react
ejecutamos el comando para instalar las dependencias    
`npm i` y despues
`npm run build`
y ejecutamos el comando:     
`npm start`

Para interactuar con el frontEnd y poder jugar se debe hacer lo siguiente:  
abrimos dos ventanas    
en la primera presionamos el boton **Crear Partida**    
en la segunda presionamos **Unirse a Partida**  
Una vez realizado este proceso ya se puede jugar el tateti entre las dos ventanas