import React, { Component } from "react";
import Tablero from "./tablero";
import Jugador from "./jugador";
import axios from "axios";

// var id1 = Math.floor(Math.random() * (10000000000 - 1)) + 1;
// var id2 = Math.floor(Math.random() * (10000000000 - 1)) + 1;
var idBoard1 = Math.floor(Math.random() * (10000000000 - 1)) + 1
var idString1: String = "1034324230";
var idString2: String = "234213";
var idBoardString: String = "1000";
var crearJuego: boolean = true;
type Jugador = {
  name: String;
  identifier: String;
};

interface IGame {
  celdas: String[];
  jugadores: Jugador[];
  turno: Jugador;
  gameFinish: Boolean;
  gameWin: Boolean;
  movimientos: Number;
}

const InitialState = {
  celdas: ["", "", "", "", "", "", "", "", ""],
  jugadores: [
    { idJugador: idString1, name: "Ignacio", identifier: "X" },
    { idJugador: idString2, name: "Juan", identifier: "O" }
  ],
  turno: { idJugador: idString1, name: "Ignacio", identifier: "X" },
  gameFinish: false,
  movimientos: 0,
  gameWin: false
};

class Game extends Component<{}, IGame> {
  // perdon por esto es asqueroso, lo iba a hacer con una lista pero se me complico
  posicion0: boolean = false;
  posicion1: boolean = false;
  posicion2: boolean = false;
  posicion3: boolean = false;
  posicion4: boolean = false;
  posicion5: boolean = false;
  posicion6: boolean = false;
  posicion7: boolean = false;
  posicion8: boolean = false;
  partidaCreada: boolean = false;
  playerTurn: String = idString1;
  puedoJugar: boolean = true;
  habilitar: boolean = false;
  public state = InitialState;
  componentDidMount() {
    axios.get('http://localhost:3000/').then(result => {
      console.log(result);
    }).catch(console.log);
    this.state.jugadores.forEach(element => {
      axios.post('http://localhost:3000/players', { idPlayer: element.idJugador, namePlayer: element.name }).then(result => {
        console.log(result);
      });
    });
  }

  public crearPartida = () => {
    axios.post(`http://localhost:3000/boards`, { idPlayer: this.state.jugadores[0].idJugador, idBoard: idBoardString }).then(result => {
      console.log(result);
    });
    this.partidaCreada = true
  }

  public unirsePartida = () => {
    axios.put(`http://localhost:3000/boards/${idBoardString}/join`, { idPlayer2: this.state.jugadores[1].idJugador }).then(result => {
      console.log(result);
    });
    this.partidaCreada = true
  }

  public verificarTabla = () => {
    axios.get(`http://localhost:3000/boards/${idBoardString}`).then(result => {
      let resultado = JSON.parse(result.data.message);
      const { turno, celdas } = this.state;
      let newCeldas = [...celdas];
      if (this.playerTurn != resultado.playerTurn){
        this.playerTurn = resultado.playerTurn;
        this.switchPlayerTurn()
      }
      if (resultado.position0 != "") {
        if (this.posicion0 === false) {
          if (resultado.position0 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[0] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion0 = true;
          } else if (resultado.position0 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[0] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion0 = true;
          }
        }
      }
      if (resultado.position1 != "") {
        if (this.posicion1 === false) {
          if (resultado.position1 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[1] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion1 = true;
          } else if (resultado.position1 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[1] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion1 = true;
          }
        }
      }
      if (resultado.position2 != "") {
        if (this.posicion2 === false) {
          if (resultado.position2 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[2] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion2 = true;
          } else if (resultado.position2 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[2] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion2 = true;
          }
        }
      }
      if (resultado.position3 != "") {
        if (this.posicion3 === false) {
          if (resultado.position3 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[3] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion3 = true;
          } else if (resultado.position3 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[3] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion3 = true;
          }
        }
      }
      if (resultado.position4 != "") {
        if (this.posicion4 === false) {
          if (resultado.position4 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[4] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion5 = true;
          } else if (resultado.position4 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[4] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion4 = true;
          }
        }
      }
      if (resultado.position5 != "") {
        if (this.posicion5 === false) {
          if (resultado.position5 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[5] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion5 = true;
          } else if (resultado.position5 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[5] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion5 = true;
          }
        }
      }
      if (resultado.position6 != "") {
        if (this.posicion6 === false) {
          if (resultado.position6 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[6] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion6 = true;
          } else if (resultado.position6 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[6] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion6 = true;
          }
        }
      }
      if (resultado.position7 != "") {
        if (this.posicion7 === false) {
          if (resultado.position7 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[7] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion7 = true;
          } else if (resultado.position7 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[7] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion7 = true;
          }
        }
      }
      if (resultado.position8 != "") {
        if (this.posicion8 === false) {
          if (resultado.position8 == resultado.idPlayer1) {
            turno.identifier = "X"
            newCeldas[8] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion8 = true;
          } else if (resultado.position8 == resultado.idPlayer2) {
            turno.identifier = "O"
            newCeldas[8] = turno.identifier
            this.setState({ celdas: newCeldas });
            this.posicion8 = true;
          }
        }
      }
    })
  }

  public switchPlayerTurn = () => {
    const { turno, jugadores } = this.state;
    //actualizar turno
    let next_player = jugadores[0];
    if (turno.identifier == "X") {
      // X [0]
      next_player = jugadores[1];
    }
    if (turno.identifier == "O") {
      //O [1]
      next_player = jugadores[0];
    }
    this.setState({ turno: next_player });
  };

  public validateGameFinish = (newCeldas: String[]) => {
    //Validar Horizontales: 0,1,2 - 3,4,5 - 6-7-8
    if (
      newCeldas[0] === newCeldas[1] &&
      newCeldas[1] === newCeldas[2] &&
      newCeldas[2] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    if (
      newCeldas[3] === newCeldas[4] &&
      newCeldas[4] === newCeldas[5] &&
      newCeldas[5] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    if (
      newCeldas[6] === newCeldas[7] &&
      newCeldas[7] === newCeldas[8] &&
      newCeldas[8] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    //Validar Verticales: 0,3,6 - 1,4,7 - 2,5,8
    if (
      newCeldas[0] === newCeldas[3] &&
      newCeldas[3] === newCeldas[6] &&
      newCeldas[6] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    if (
      newCeldas[1] === newCeldas[4] &&
      newCeldas[4] === newCeldas[7] &&
      newCeldas[7] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    if (
      newCeldas[2] === newCeldas[5] &&
      newCeldas[5] === newCeldas[8] &&
      newCeldas[8] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    //Validar Diagonales 0,4,8  - 2,4,6
    if (
      newCeldas[0] === newCeldas[4] &&
      newCeldas[4] === newCeldas[8] &&
      newCeldas[8] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    if (
      newCeldas[2] === newCeldas[4] &&
      newCeldas[4] === newCeldas[6] &&
      newCeldas[6] !== ""
    ) {
      this.switchPlayerTurn();
      this.setState({ gameFinish: true, gameWin: true });
      return true;
    }
    return false;
  };

  public handleCelClick = (index: any) => () => {
    const { turno, celdas, gameFinish } = this.state;
    axios.put(`http://localhost:3000/boards/${idBoardString}/movement`, { position: index }).then(result => {
      console.log(result)
    });
    if (!gameFinish && celdas[index] === "" && this.puedoJugar === true) {
      this.puedoJugar = false
      let newCeldas = [...celdas];
      //actualizar tablero
      newCeldas[index] = turno.identifier;
      this.setState({ celdas: newCeldas});
      // const gameFinish = this.validateGameFinish(newCeldas);
      // if (!gameFinish) {
      //   this.switchPlayerTurn();
      // }
    }
  };
  public verificarGanador = () =>
  {
    const { turno, celdas } = this.state;
    let newCeldas = [...celdas];
    let movimientos = this.state.movimientos;
    for(var i = 0; i < 9; i++){
      const gameFinish = this.validateGameFinish(newCeldas);
      if (newCeldas[i] != ""){
        movimientos++;
      }
      if (gameFinish) {
        this.partidaCreada = false;
      }
    }
    if (movimientos != 9){
      movimientos = 0
    } else{
      this.partidaCreada = false
      this.setState({ gameFinish: true });
    }
  }
  public restartGame = () => {
    this.setState(InitialState);
  };


  render() {
    const { turno, celdas, jugadores, gameFinish, gameWin } = this.state;
    setInterval(() => {
        if(this.partidaCreada === true){
        this.verificarTabla();
        this.verificarGanador();
        console.log("holaaa")
         }
    }, 2500)
    return (
      <div>
        {
         
        gameWin
          ? `Juego ganado por ${turno.name}`
          : gameFinish
            ? "Nadie ganó el juego"
          : `Turno de ${turno.name}`}
        <br />
        {<button onClick={this.crearPartida}>Crear Partida</button>}
        <br />
        {<button onClick={this.unirsePartida}>Unirse a Partida</button>}
        <br /> <br />
        
        <Tablero celdas={celdas} handleCelClick={this.handleCelClick} />
        {jugadores.map((jugador, i) => (
          <Jugador
            idJugador={jugador.idJugador}
            name={jugador.name}
            identifier={jugador.identifier}
          />
        ))}

      </div>
    );
  }
}

export default Game;
