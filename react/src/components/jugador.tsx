import * as React from "react";
import { render } from "react-dom";

type JugadorProps = {
    idJugador: String,
    name: String,
    identifier: String,
}

const Jugador = (props: JugadorProps) => {
    return <div>
        <p> id: {props.idJugador}</p>
        <b> Jugador: {props.name} </b>
        <p> Identificador: {props.identifier}</p>
    </div>
}
export default Jugador;
